tzutil /s "SE Asia Standard Time"
net stop w32time
w32tm /unregister
w32tm /register
net start w32time
w32tm /resync
w32tm /config /manualpeerlist:"pool.ntp.org,0x1" /syncfromflags:manual /reliable:yes /update
w32tm /config /update
w32tm /resync /force
pause